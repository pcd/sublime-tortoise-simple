import sublime
import sublime_plugin
import os.path
import subprocess

tortoise_svn_path = 'C:\\Program Files\\TortoiseSVN\\bin\\TortoiseProc.exe'
tortoise_git_path = 'C:\\Program Files\\TortoiseGit\\bin\\TortoiseGitProc.exe'

def plugin_loaded():
	global tortoise_svn_path 
	global tortoise_git_path 
	try:
		settings = sublime.load_settings('Tortoise-simple.sublime-settings')
		tortoise_svn_path = settings.get('tortoise_svn_path')
		tortoise_git_path = settings.get('tortoise_git_path') 
	except Exception as e:
		sublime.message_dialog('Tortoise-simple: Error reading settings');
		raise

def get_tortoise_type(path):
	prev, current = None, path if os.path.isdir(path) else os.path.dirname(path)
	while current != prev:
		if os.path.exists(os.path.join(current, '.git')):
			return 'git'
		if os.path.exists(os.path.join(current, '.svn')):
			return 'svn'
		prev, current = current, os.path.dirname(current)
	return None

def run_tortoise(tortoisePath, command, filepath, line=0):
	cmdString = '"{0}" /command:{1} /path:"{2}"'
	if line != 0:
		cmdString = cmdString + '/line:{3}'
	cmd = cmdString.format(tortoisePath, command, filepath, line)
	try:
		subprocess.Popen(cmd)
	except Exception:
		sublime.message_dialog('Unable to run command: ' + cmd)
		raise

class SimpleTortoiseCommand():
	def run_command(self, command, filepaths, line=0):
		filepath = filepaths[0] if (filepaths and filepaths[0]) else self.window.active_view().file_name()
		type = get_tortoise_type(filepath)
		if type == 'git':
			run_tortoise(tortoise_git_path, command, filepath, line);
		elif type == 'svn':
			run_tortoise(tortoise_svn_path, command, filepath, line);
		# TODO: hg
		else:
			sublime.message_dialog('File not in a repository: ' + filepath)

class SimpleTortoiseLogCommand(sublime_plugin.WindowCommand, SimpleTortoiseCommand):
	def run(self, paths):
		self.run_command('log', paths)

class SimpleTortoiseDiffCommand(sublime_plugin.WindowCommand, SimpleTortoiseCommand):
	def run(self, paths):
		(row,col) = self.window.active_view().rowcol(self.window.active_view().sel()[0].begin())
		self.run_command('diff', paths, row + 1)

class SimpleTortoiseBlameCommand(sublime_plugin.WindowCommand, SimpleTortoiseCommand):
	def run(self, paths):
		(row,col) = self.window.active_view().rowcol(self.window.active_view().sel()[0].begin())
		self.run_command('blame', paths, row + 1)

class SimpleTortoiseAddCommand(sublime_plugin.WindowCommand, SimpleTortoiseCommand):
	def run(self, paths):
		self.run_command('add', paths)

class SimpleTortoiseRevertCommand(sublime_plugin.WindowCommand, SimpleTortoiseCommand):
	def run(self, paths):
		self.run_command('revert', paths)

class SimpleTortoiseCommitCommand(sublime_plugin.WindowCommand, SimpleTortoiseCommand):
	def run(self, paths):
		self.run_command('commit', paths)
